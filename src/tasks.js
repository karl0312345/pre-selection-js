// --------------------------------- 1
const getChars = () => {
    // Student implementation
    return;
}

// --------------------------------- 2
const char_freq = () => {
    // Student implementation
    return;
}

// --------------------------------- 3
const range = () => {
    // Student implementation
    return;
}

// --------------------------------- 4
const listNames = () => {
    // Student implementation
    return;
}

// --------------------------------- 5
const keysAndValues = () => {
    // Student implementation
    return;
}

// --------------------------------- 6

const swapMinMax = () => { 
    // Student implementation
    return;
}  

// --------------------------------- 7
const fizzBuzz = () => {
    // Student implementation
}

// --------------------------------- 8
const add = () => {
    // Student implementation
    return;
}

// --------------------------------- 9

const multiplyArrayItems = () => {
    // Student implementation
    return;
}

// --------------------------------- 10
const uniqueValues = () => {
    // Student implementation
    return; 
}


module.exports = {
    getChars,
    char_freq,
    range,
    listNames,
    keysAndValues,
    swapMinMax,
    fizzBuzz,
    add,
    multiplyArrayItems,
    uniqueValues
}